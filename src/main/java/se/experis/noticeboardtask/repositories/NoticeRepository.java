package se.experis.noticeboardtask.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboardtask.models.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(int id);
}
