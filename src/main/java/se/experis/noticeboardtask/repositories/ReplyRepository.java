package se.experis.noticeboardtask.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboardtask.models.Reply;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Reply getById(int id);
}
