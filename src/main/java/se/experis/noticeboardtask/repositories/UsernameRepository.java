package se.experis.noticeboardtask.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboardtask.models.Username;

//This repository makes it possible to find a user (called username because I am not sure about using user as a var-name. maybe not a problem?), either by ID or by its username
public interface UsernameRepository extends JpaRepository<Username, Integer> {
    Username getById(int id);
}
