package se.experis.noticeboardtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoticeboardtaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoticeboardtaskApplication.class, args);
	}

}
