package se.experis.noticeboardtask.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Notice implements Comparator<Notice>{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String noticeTitle;

    @Column
    Date date = Calendar.getInstance().getTime();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public String strDateNotice = dateFormat.format(date);

    @Column(columnDefinition = "text" )
    public String noticeText;

    @Column
    public String username;


    //compare strdate (date) and will sort it so that the newest date is first
    @Override
    public int compare(Notice o1, Notice o2) {
        return o2.strDateNotice.compareTo(o1.strDateNotice);
    }

    @OneToMany(targetEntity=Reply.class,cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true )

    public Set<Reply> replies = new HashSet<>();

}



