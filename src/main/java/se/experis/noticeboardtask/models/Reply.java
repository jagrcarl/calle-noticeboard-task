package se.experis.noticeboardtask.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    public class Reply {

        //automatically creates a new id
        @Id
        @GeneratedValue(strategy= GenerationType.IDENTITY)
        public Integer id;

        @Column(nullable = false)
        public String replyTitle;

        @Column(columnDefinition = "text" )
        public String replyText;

        @Column
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        public String strDateReply = dateFormat.format(date);

        @Column //not used in this version
        public String username;

        @ManyToOne(fetch = FetchType.LAZY)
        private Notice notice;
}
