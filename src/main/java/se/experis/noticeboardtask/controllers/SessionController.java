package se.experis.noticeboardtask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboardtask.models.Username;
import se.experis.noticeboardtask.repositories.UsernameRepository;
import se.experis.noticeboardtask.utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class SessionController {

    @Autowired
    private UsernameRepository usernameRepository;

    //this will create a session
    //it take the data the user typed in on the login page, checks if the password is correct
    //and then stores the username in a cookie
    @PostMapping(value = "/saveSession")
    public String add(
            @RequestBody Username usernameBody,
            HttpServletResponse response,
            HttpSession session) {
        if (usernameBody.password.equals("cat")) {                     //if password is corrct, add session.
            System.out.println("Logged in with " + usernameBody.username + ". Password were correct");
            SessionKeeper.getInstance().AddSession(session.getId());

            Cookie cookieUsername = new Cookie("username", usernameBody.username);
            cookieUsername.setMaxAge(600); //Session active for 10 minutes
            response.addCookie(cookieUsername);
        } else {
            System.out.println("Login failed.");
        }
        return "redirect:/";
    }

    //maps the login page
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    //clear session and cookies when the user logs off
    @PostMapping("/logout")
    public String logut(HttpServletResponse response, HttpSession session) {
        SessionKeeper.getInstance().RemoveSession(session.getId());

        Cookie cookieUsername = new Cookie("cookieUsername", "");
        cookieUsername.setMaxAge(0);
        response.addCookie(cookieUsername);
        System.out.println("Logged off");

        return "redirect:/";
    }

}
