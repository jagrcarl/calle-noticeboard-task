package se.experis.noticeboardtask.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboardtask.models.Notice;
import se.experis.noticeboardtask.models.Reply;
import se.experis.noticeboardtask.repositories.NoticeRepository;
import se.experis.noticeboardtask.repositories.ReplyRepository;
import se.experis.noticeboardtask.utils.SessionKeeper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@RestController
public class ReplyController {

    @Autowired
    private ReplyRepository replyRepository;

    @Autowired
    private NoticeRepository noticeRepository;

    HttpStatus resp;

    //this creates a new reply. it is linked to a specific entry (notice id)
    @PostMapping("/reply-add/{noticeId}")
    public ResponseEntity<Reply> createNewReplyIntoNotice(@RequestBody Reply reply, @PathVariable Integer noticeId, HttpSession session) {

        if(noticeRepository.existsById(noticeId) &&  SessionKeeper.getInstance().CheckSession(session.getId())) {
            Optional<Notice> optionalNotice = noticeRepository.findById(noticeId);
            Notice notice = optionalNotice.get();
            notice.replies.add(reply);
            notice = noticeRepository.save(notice);
            resp = HttpStatus.CREATED;
        } else if(!SessionKeeper.getInstance().CheckSession(session.getId())) {
            System.out.println("User has not signed in.");
            resp = HttpStatus.NOT_FOUND;
        } else {
            System.out.println("Something went wrong");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(reply, resp);
    }

    //will show a specific reply
    @GetMapping("/reply/{id}")
    public ResponseEntity<Reply> getEntryById(HttpServletRequest request, @PathVariable Integer id){

        Reply reply;

        if(replyRepository.existsById(id)) {
            System.out.println("Reply with id: " + id);
            reply = replyRepository.getById(id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Reply not found");
            reply = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(reply, resp);
    }

    //this will update the reply with a specific id. is not used in this version.
//    @PatchMapping("/reply/{id}")
//    public ResponseEntity<Reply> updateReply(HttpServletRequest request, @RequestBody Reply newReply, @PathVariable Integer id) {
//
//        Reply reply = null;
//        HttpStatus resp;
//
//        if(replyRepository.existsById(id)) {
//            Optional<Reply> entryRepo = replyRepository.findById(id);
//            reply = entryRepo.get();
//
//            if(newReply.replyTitle != null) {
//                reply.replyTitle = newReply.replyTitle;
//            }
//
//            if(newReply.replyTitle != null) {
//                reply.replyText = newReply.replyText;
//            }
//
//            replyRepository.save(reply);
//
//            System.out.println("Updated Reply with id: " + reply.id);
//            resp = HttpStatus.OK;
//        } else {
//            System.out.println("Reply not found with id: " + id);
//            resp = HttpStatus.NOT_FOUND;
//        }
//
//        return new ResponseEntity<>(reply, resp);
//    }

    //this will delete a reply with a specific id. is not used in this version
//    @DeleteMapping("/reply/{id}")
//    public ResponseEntity<String> deleteEntry(HttpServletRequest request, HttpSession session, @PathVariable Integer id) {
//
//        HttpStatus resp;
//        String message = "";
//
//        if(replyRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId())) {
//            replyRepository.deleteById(id);
//            System.out.println("Deleted Reply with id: " + id);
//            message = "SUCCESS";
//            resp = HttpStatus.OK;
//        }
//        else {
//            System.out.println("Reply not found with id: " + id);
//            message = "FAIL";
//            resp = HttpStatus.NOT_FOUND;
//        }
//
//        return new ResponseEntity<>(message, resp);
//    }
}
