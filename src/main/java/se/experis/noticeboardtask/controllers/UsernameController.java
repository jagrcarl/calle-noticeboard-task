package se.experis.noticeboardtask.controllers;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.experis.noticeboardtask.models.Notice;
import se.experis.noticeboardtask.models.Username;
import se.experis.noticeboardtask.repositories.UsernameRepository;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UsernameController {

    @Autowired
    private UsernameRepository usernameRepository;

    //makes it possible to create a user
    @PostMapping("/username")
    public ResponseEntity<Username> addUsername(HttpServletRequest request, @RequestBody Username username){

        username = usernameRepository.save(username);

        System.out.println("New User with id: " + username.id);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(username, resp);
    }

}
