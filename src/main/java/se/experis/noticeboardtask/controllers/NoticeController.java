package se.experis.noticeboardtask.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboardtask.models.Notice;
import se.experis.noticeboardtask.repositories.NoticeRepository;
import se.experis.noticeboardtask.utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@RestController
public class NoticeController {

    Notice notice = null;
    HttpStatus resp;

    @Autowired
    private NoticeRepository noticeRepository;

    //will load the specific entry with the help of notice id
    @GetMapping("/notice/{id}")
    public ResponseEntity<Notice> getNoticeById(HttpServletRequest request, @PathVariable Integer id){

        if(noticeRepository.existsById(id)) {
            System.out.println("Entry with id: " + id);
            notice = noticeRepository.getById(id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found");
            notice = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(notice, resp);
    }

    //handles so a user can post a new entry
    //it requires that the uses has signed in and it will find the username and put as a value in notice.username
    @PostMapping("/notice")
    public ResponseEntity<Notice> addNotice(HttpServletRequest request, HttpSession session, @RequestBody Notice notice){

        if(SessionKeeper.getInstance().CheckSession(session.getId())) {
            Cookie[] findCookie = request.getCookies();

            for (Cookie c : findCookie) {
                if (c.getName().equals("username")) {
                    notice.username = c.getValue();
                    System.out.println(c.getValue());
                }
            }
            notice = noticeRepository.save(notice);

            System.out.println("New entry with id: " + notice.id);

            resp = HttpStatus.CREATED;
        } else {
            resp = HttpStatus.FORBIDDEN;
            System.out.println("User has not signed in.");
        }

        return new ResponseEntity<>(notice, resp);
    }

    //this will update the entries with a specific id.
    //it controls that the user has signed in and that the username correlate with notice.username
    @PatchMapping("/notice/{id}")
    public ResponseEntity<Notice> updateNotice(HttpServletRequest request, HttpSession session, @RequestBody Notice newNotice, @PathVariable Integer id) {

        String usernameCookie = "";
        Cookie[] findCookie = request.getCookies();

        for(Cookie c : findCookie) {
            if(c.getName().equals("username")) {
                usernameCookie = c.getValue();
                System.out.println(usernameCookie);
            }
        }
        notice = noticeRepository.getById(id);


        if(noticeRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId()) && usernameCookie.equals(notice.username)) {
            Optional<Notice> noticeRepo = noticeRepository.findById(id);
            notice = noticeRepo.get();

            if(newNotice.noticeTitle != null) { //cant be null
                notice.noticeTitle = newNotice.noticeTitle;
            }

            if(newNotice.noticeText != null) { //cant be null
                notice.noticeText = newNotice.noticeText;
            }
            
            noticeRepository.save(notice);

            System.out.println("Updated Entry with id: " + notice.id);
            resp = HttpStatus.OK;
        } else if (!usernameCookie.equals(notice.username)) {
            System.out.println("Wrong user!");
            resp = HttpStatus.NOT_FOUND;
        } else {
            System.out.println("Entry not found with id: " + id);
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(notice, resp);
    }

    //this will delete a entry with a specific id.
    //it controls that the user has signed in and that the username correlate with notice.username
    @DeleteMapping("/notice/{id}")
    public ResponseEntity<String> deleteNotice(HttpServletRequest request, HttpSession session, @PathVariable Integer id) {

        String message = "";
        String usernameCookie = "";
        Cookie[] findCookie = request.getCookies();

        for(Cookie c : findCookie) {
            if(c.getName().equals("username")) {
                usernameCookie = c.getValue();
                System.out.println(usernameCookie);
            }
        }
        notice = noticeRepository.getById(id);

        if(noticeRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId()) && usernameCookie.equals(notice.username)) {
            noticeRepository.deleteById(id);
            System.out.println("Deleted Entry with id: " + id);
            message = "SUCCESS";
            resp = HttpStatus.OK;
        } else if (!usernameCookie.equals(notice.username)) {
            System.out.println("Wrong user!");
            message = "WRONG USER";
            resp = HttpStatus.NOT_FOUND;
        } else {
            System.out.println("Entry not found with id: " + id);
            message = "FAIL";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(message, resp);
    }
}
