package se.experis.noticeboardtask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.noticeboardtask.models.Notice;
import se.experis.noticeboardtask.models.Reply;
import se.experis.noticeboardtask.repositories.NoticeRepository;
import se.experis.noticeboardtask.repositories.ReplyRepository;
import java.util.List;

@Controller
public class ThymeleafController {

    @Autowired
    NoticeRepository noticeRepository;

    @Autowired
    ReplyRepository replyRepository;

        //maps noties-view. List <Notice> gets everything in the Notice class, sorts it by date and returns it
    @GetMapping("/notices-view")
    public String noticeView(Model model) {

        List<Notice> notice = noticeRepository.findAll();
        notice.sort(new Notice());
        model.addAttribute("notices",notice);

        return "notices";
    }

    //maps reply-view. the page will show all replies
    @GetMapping("/reply-view")
    public String replyView(Model model) {

        List<Reply> reply = replyRepository.findAll();
        model.addAttribute("replies",reply);

        return "replies";
    }

    //same as reply-view, but show a specific reply
    @GetMapping("/view-reply/{id}")
    public String viewReply(@PathVariable int id, Model model) {
        Reply reply = replyRepository.getById(id);
        model.addAttribute("replies", reply);

        return "viewreply";
    }

    //maps add-notice, where the user enter text for an entry
    @GetMapping("/add-notice")
    public String addNotice(Model model) {
        return "addnotice";
    }

    //maps a specific entry
    @GetMapping("/view-notices/{id}")
    public String viewNotice(@PathVariable int id, Model model) {
        Notice notice = noticeRepository.getById(id);
        model.addAttribute("notices", notice);

        return "viewnotice";
    }

    //maps a add page for reply
    @GetMapping("/add-reply/{id}")
    public String addReply(@PathVariable int id, Model model) {
        Notice notice = noticeRepository.getById(id);
        model.addAttribute("notices", notice);

        return "addreply";
    }

    //maps a page that can edit a specific entry
    @GetMapping("/edit-notices/{id}")
    public String editNotice(@PathVariable int id, Model model) {
        Notice notice = noticeRepository.getById(id);
        model.addAttribute("notices", notice);
        return "editnotice";
    }

    //maps add-reply
    @GetMapping("/add-reply")
    public String addReply(Model model) {
        return "addreply";
    }
}
